<?php

namespace Drupal\google_cloud_vision_media\Exceptions;

/**
 * Class LabelAlreadyExistsException.
 *
 * @package Drupal\google_cloud_vision_media\Exceptions
 */
class LabelAlreadyExistsException extends \RuntimeException {}
